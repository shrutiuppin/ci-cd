var gulp = require('gulp');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var runSequence = require('run-sequence');
var gulpCopy = require('gulp-copy');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var del = require('del');
var htmlmin = require('gulp-htmlmin');
var uglify = require('gulp-uglify');
var babel = require('gulp-babel');

var source = 'source/';
var destination = 'destination/';

const AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

gulp.task('copyImage',function(){
    return gulp
        .src(source+'dahlias-red-white.jpg')
        .pipe(gulp.dest(destination))
})

gulp.task('pugToHtml',function() {
    return gulp.src(source+'index.pug')
        .pipe(pug({
        doctype: 'html',
        pretty: true
    }))
        .pipe(htmlmin({
        collapseWhitespace: true,
        removeComments: true
    }))
        .pipe(gulp.dest(destination));
});

gulp.task('scssToCSS', function(){
    gulp.src(source+'main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
        .pipe(csso())
        .pipe(gulp.dest(destination));  
});

gulp.task('toES6',function(){
    gulp.src(source+'app.js')
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulp.dest(destination))
});

gulp.task('clean', () => del(destination));

gulp.task('default', ['clean'],function () {
    runSequence(
        'copyImage', 'pugToHtml','scssToCSS','toES6'
    );
});