# ci-cd

Homepage that is implemented using all the aspects of front end web development

# Pre-requisites
- Nodejs
- npm
- git

# Steps to set up

```sh
$ git clone https://bitbucket.org/shrutiuppin/ci-cd.git
$ cd ci-cd
$ npm install
$ gulp
```
Respective files will be generated in destination folder